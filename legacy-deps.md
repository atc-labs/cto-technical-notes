# Analysing Legacy Dependencies in Dashboard

## Install src for Dashboard

Let's try and install from src / gitlab

```
⠵ npm install
npm ERR! code ERESOLVE
npm ERR! ERESOLVE could not resolve
npm ERR!
npm ERR! While resolving: react-bootstrap-table-next@4.0.3
npm ERR! Found: react-dom@17.0.2
npm ERR!
npm ERR! Could not resolve dependency:
npm ERR! peer react-dom@"^16.3.0" from react-bootstrap-table-next@4.0.3
npm ERR!
npm ERR! Fix the upstream dependency conflict, or retry
npm ERR! this command with --force or --legacy-peer-deps
npm ERR! to accept an incorrect (and potentially broken) dependency resolution.
```
Fatal error, we cannot install as react-bootstrap-table-next needs react 16.3.0, as of writing the latest major version is 18.2.0. The reason for this is that react-bootstrap-table-next is no longer maintained, the last code update was more than 3 years ago.

So we follow the instructions given by node package manager (npm) and use the command to install legacy dependencies.

```
⠵ npm install --legacy-peer-deps
added 1843 packages, and audited 2092 packages in 14s

17 vulnerabilities (9 moderate, 6 high, 2 critical)

To address issues that do not require attention, run:
  npm audit fix

To address all issues (including breaking changes), run:
  npm audit fix --force
```

This now install without fatal error but results in 17 total vulnerabilities, 2 of which are critical. 

So let's try and remediate as a junior developer would, we ask it to check the audit and fix the issues (spoiler, it's very bad at this).

```
⠵ npm audit fix --force
Will install react-scripts@5.0.1, which is a breaking change
npm WARN ERESOLVE overriding peer dependency
npm WARN While resolving: react-bootstrap-table2-paginator@2.1.2
npm WARN Could not resolve dependency:
npm WARN Conflicting peer dependency: react-dom@16.14.0

90 vulnerabilities (14 low, 25 moderate, 43 high, 8 critical)
```

This appears to introduce a breaking change and raises the total number of vulnerabilities to 90, including 8 critical ones, up from 2, as well as 43 high ones, up from 6.

Let's try and run.

```
Error: error:0308010C:digital envelope routines::unsupported
    at new Hash (node:internal/crypto/hash:69:19)
    at Object.createHash (node:crypto:138:10)
    at module.exports (/Users/m/Developer/AtumScan/Dashboard/node_modules/webpack/lib/util/createHash.js:90:53)
    at NormalModule._initBuildHash (/Users/m/Developer/AtumScan/Dashboard/node_modules/webpack/lib/NormalModule.js:386:16)
    at /Users/m/Developer/AtumScan/Dashboard/node_modules/webpack/lib/NormalModule.js:418:10
    at /Users/m/Developer/AtumScan/Dashboard/node_modules/webpack/lib/NormalModule.js:293:13
    at /Users/m/Developer/AtumScan/Dashboard/node_modules/loader-runner/lib/LoaderRunner.js:367:11
    at /Users/m/Developer/AtumScan/Dashboard/node_modules/loader-runner/lib/LoaderRunner.js:233:18
    at context.callback (/Users/m/Developer/AtumScan/Dashboard/node_modules/loader-runner/lib/LoaderRunner.js:111:13)
    at /Users/m/Developer/AtumScan/Dashboard/node_modules/babel-loader/lib/index.js:51:103 {
  opensslErrorStack: [ 'error:03000086:digital envelope routines::initialization error' ],
  library: 'digital envelope routines',
  reason: 'unsupported',
  code: 'ERR_OSSL_EVP_UNSUPPORTED'
}

Node.js v20.3.1
ERROR: "spa" exited with 1.
```

As warned, it did indeed break.

## Importance of Latest Stable Releases
The importance of the latest stable releases cannot be overstated. These releases play a critical role in ensuring the reliability, security, and performance of software systems. Here are some key reasons why staying up to date with the latest stable releases is vital:

1. Security: Software vulnerabilities and bugs are continuously discovered, and the latest stable releases often contain crucial security patches and fixes. By promptly installing these updates, users can protect their systems from potential threats and reduce the risk of cyberattacks or data breaches.

2. Stability and Reliability: Stable releases undergo rigorous testing and bug fixing processes, resulting in improved stability and reliability. Each new release typically addresses known issues, enhances performance, and provides a smoother user experience. By upgrading to the latest stable version, users can enjoy a more robust and dependable software environment.

3. New Features and Enhancements: Stable releases often introduce new features, functionality, and improvements. These additions can enhance productivity, efficiency, and user satisfaction. By adopting the latest stable releases, users can benefit from the latest innovations and take advantage of valuable enhancements that could positively impact their workflows or daily activities.

4. Compatibility: As software evolves, compatibility with other systems, platforms, or dependencies may change. The latest stable releases are designed to maintain compatibility with current technologies and frameworks. By keeping up with these releases, users can ensure seamless integration with other software components, avoid compatibility issues, and maximize the efficiency of their systems.

5. Community Support: Community support and assistance for older software versions gradually decrease as focus shifts to newer releases. By staying updated with the latest stable releases, users can benefit from active community support, access to forums, documentation, and a broader range of resources. This helps to troubleshoot issues, find solutions, and stay connected with a vibrant community of users and developers.

In summary, embracing the latest stable releases is essential for maintaining security, stability, compatibility, and accessing new features and improvements. By staying current, users can safeguard their systems, optimize performance, and fully leverage the potential of their software.
