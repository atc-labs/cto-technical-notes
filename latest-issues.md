# 15 vulnerabilities (9 moderate, 6 high)

To address issues that do not require attention, run:
  npm audit fix

To address all issues (including breaking changes), run:
  npm audit fix --force

## npm audit report

jsonwebtoken  <=8.5.1
Severity: moderate
jsonwebtoken unrestricted key type could lead to legacy keys usage  - https://github.com/advisories/GHSA-8cf7-32gw-wr33
jsonwebtoken's insecure implementation of key retrieval function could lead to Forgeable Public/Private Tokens from RSA to HMAC - https://github.com/advisories/GHSA-hjrf-2m68-5959
jsonwebtoken vulnerable to signature validation bypass due to insecure default algorithm in jwt.verify() - https://github.com/advisories/GHSA-qwph-4952-7xr6
fix available via `npm audit fix --force`
Will install jwks-rsa@3.0.1, which is a breaking change
node_modules/jsonwebtoken
  express-jwt  <=7.7.7
  Depends on vulnerable versions of jsonwebtoken
  node_modules/express-jwt
  jwks-rsa  1.5.1 - 1.12.3
  Depends on vulnerable versions of jsonwebtoken
  node_modules/jwks-rsa

```
  ╰─⠠⠵ grep -r jsonwebtoken . | cut -d ":" -f1 | sort -u
./node_modules/.package-lock.json
./node_modules/@auth0/auth0-spa-js/package.json
./node_modules/ecdsa-sig-formatter/package.json
./node_modules/express-jwt/CHANGELOG.md
./node_modules/express-jwt/README.md
./node_modules/express-jwt/lib/index.js
./node_modules/express-jwt/package.json
./node_modules/express-jwt/test/jwt.test.js
./node_modules/express-jwt/test/multitenancy.test.js
./node_modules/express-jwt/test/revocation.test.js
./node_modules/express-jwt/test/string_token.test.js
./node_modules/jsonwebtoken/CHANGELOG.md
./node_modules/jsonwebtoken/README.md
./node_modules/jsonwebtoken/package.json
./node_modules/jwks-rsa/CHANGELOG.md
./node_modules/jwks-rsa/lib/integrations/passport.js
./node_modules/jwks-rsa/package.json
./node_modules/jwks-rsa/tests/mocks/tokens.js
./node_modules/jwks-rsa/yarn-error.log
./package-lock.json
./yarn.lock
```


nth-check  <2.0.1
Severity: high
Inefficient Regular Expression Complexity in nth-check - https://github.com/advisories/GHSA-rp65-9cf3-cjxr
fix available via `npm audit fix --force`
Will install react-scripts@2.1.3, which is a breaking change
node_modules/svgo/node_modules/nth-check
  css-select  <=3.1.0
  Depends on vulnerable versions of nth-check
  node_modules/svgo/node_modules/css-select
    svgo  1.0.0 - 1.3.2
    Depends on vulnerable versions of css-select
    node_modules/svgo
      @svgr/plugin-svgo  <=5.5.0
      Depends on vulnerable versions of svgo
      node_modules/@svgr/plugin-svgo
        @svgr/webpack  4.0.0 - 5.5.0
        Depends on vulnerable versions of @svgr/plugin-svgo
        node_modules/@svgr/webpack
          react-scripts  >=2.1.4
          Depends on vulnerable versions of @svgr/webpack
          node_modules/react-scripts

```
./node_modules/.package-lock.json
./node_modules/css-select/lib/pseudo-selectors/filters.js
./node_modules/css-select/package.json
./node_modules/nth-check/README.md
./node_modules/nth-check/lib/compile.d.ts
./node_modules/nth-check/lib/compile.d.ts.map
./node_modules/nth-check/lib/compile.js
./node_modules/nth-check/lib/compile.js.map
./node_modules/nth-check/lib/esm/compile.d.ts
./node_modules/nth-check/lib/esm/compile.d.ts.map
./node_modules/nth-check/lib/esm/compile.js
./node_modules/nth-check/lib/esm/compile.js.map
./node_modules/nth-check/lib/esm/index.d.ts.map
./node_modules/nth-check/lib/esm/index.js.map
./node_modules/nth-check/lib/esm/parse.d.ts.map
./node_modules/nth-check/lib/esm/parse.js.map
./node_modules/nth-check/lib/index.d.ts.map
./node_modules/nth-check/lib/index.js.map
./node_modules/nth-check/lib/parse.d.ts.map
./node_modules/nth-check/lib/parse.js.map
./node_modules/nth-check/package.json
./node_modules/svgo/node_modules/css-select/lib/pseudos.js
./node_modules/svgo/node_modules/css-select/package.json
./node_modules/svgo/node_modules/nth-check/README.md
./node_modules/svgo/node_modules/nth-check/package.json
./node_modules/svgo/node_modules/nth-check/parse.js
./node_modules/tailwindcss/peers/index.js
./package-lock.json
```


semver  <=5.7.1 || 6.0.0 - 6.3.0 || 7.0.0 - 7.5.1
Severity: moderate
semver vulnerable to Regular Expression Denial of Service - https://github.com/advisories/GHSA-c2qf-rxjj-qqgw
semver vulnerable to Regular Expression Denial of Service - https://github.com/advisories/GHSA-c2qf-rxjj-qqgw
semver vulnerable to Regular Expression Denial of Service - https://github.com/advisories/GHSA-c2qf-rxjj-qqgw
fix available via `npm audit fix`
node_modules/@typescript-eslint/eslint-plugin/node_modules/semver
node_modules/@typescript-eslint/typescript-estree/node_modules/semver
node_modules/@typescript-eslint/utils/node_modules/semver
node_modules/cross-spawn/node_modules/semver
node_modules/css-loader/node_modules/semver
node_modules/fork-ts-checker-webpack-plugin/node_modules/semver
node_modules/jest-snapshot/node_modules/semver
node_modules/jsonwebtoken/node_modules/semver
node_modules/nodemon/node_modules/semver
node_modules/normalize-package-data/node_modules/semver
node_modules/postcss-loader/node_modules/semver
node_modules/react-scripts/node_modules/semver
node_modules/semver
node_modules/simple-update-notifier/node_modules/semver
  simple-update-notifier  1.0.7 - 1.1.0
  Depends on vulnerable versions of semver
  node_modules/simple-update-notifier
    nodemon  2.0.19 - 2.0.22
    Depends on vulnerable versions of simple-update-notifier
    node_modules/nodemon

tough-cookie  <4.1.3
Severity: moderate
tough-cookie Prototype Pollution vulnerability - https://github.com/advisories/GHSA-72xf-g2v4-qvf3
fix available via `npm audit fix`
node_modules/tough-cookie

word-wrap  *
Severity: moderate
word-wrap vulnerable to Regular Expression Denial of Service - https://github.com/advisories/GHSA-j8xg-fqg3-53r7
fix available via `npm audit fix`
node_modules/word-wrap
  optionator  0.8.3 - 0.9.1
  Depends on vulnerable versions of word-wrap
  node_modules/eslint/node_modules/optionator
  node_modules/optionator

15 vulnerabilities (9 moderate, 6 high)

To address issues that do not require attention, run:
  npm audit fix

To address all issues (including breaking changes), run:
  npm audit fix --force
